#!/bin/sh
#Build Server Docker container
#

# Usage EXAMPLE: ./build-server.sh ubuntu18.04
distro="$(tr [A-Z] [a-z] <<< "$1")" # set to lowercase

    # Set build directory
    build_dir='./server'
    echo ">>: "$distro
    # remove Dockerfile here (if exists)
    # rm $build_dir/Dockerfile || true
     
     rm -rf $build_dir/* $build_dir/.dockerignore || true

    # copy desired Dockerfile
    cp -R ../Dockerfiles/$distro/ $build_dir
    #cp -R container/ $build_dir/container
    #cp -R html/ $build_dir/html
    #cp -R snippets/ $build_dir/snippets
    #cp -R certs/ $build_dir/certs
    #cp -R includes/ $build_dir/includes

    #ls $build_dir
    

    # Build and tag it as "nginx-plus-[distro]"
    docker build -t $distro -t localhost:5005/$distro $build_dir --pull --no-cache # No caching
    docker push localhost:5005/server-$distro

    # Show all docker containers build with names containing "nginx-plus-"
    printf "\n"
    printf "Server containers built:"
    printf "\n"
    docker images | grep server

    # remove Dockerfile from the build directory (if exists)
    rm -rf $build_dir/* $build_dir/.dockerignore || true